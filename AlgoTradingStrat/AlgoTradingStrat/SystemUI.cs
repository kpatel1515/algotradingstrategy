﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace AlgoTradingStrat
{
    public delegate void MessageUpdate(String message);
    public delegate void SPuDSMessageUpdate(String message, String stock);
    public delegate void SPuDSMarketUpdate(double price, String stock);
    public delegate void SPuDSPandLUpdate(double pandl, String stock);
    public delegate void SPuDSStateUpdate(String state, String stock);

    public partial class SystemUI : Form
    {
        private SystemManager _SysMan;
        private Dictionary<String, SPuDS_StratUI> _SPuDS_Pages;
        private bool _IBdisconnected = true;

        private MessageUpdate MainMessageBoxUpdateHandeler;
        private SPuDSMessageUpdate SPuDSMessageBoxUpdateHandeler;
        private SPuDSMarketUpdate SPuDSMarketChartUpdateHandeler;
        private EnableButton Enable_AddSymHandeler;
        private SPuDSPandLUpdate SPuDSPandLUpdateHandeler;
        private SPuDSStateUpdate SPuDSStateUpdateHandeler;

        public SystemUI()
        {
            InitializeComponent();
            _SysMan = SystemManager.CreateSystemManager();
            _SysMan.CreateIBApi();
            _SysMan.StartSPuDS();
            _SysMan._MainMessageBoxUpdate += _SysMan__MainMessageBoxUpdate;
            _SysMan._SPuDSMessageUpdate += _SysMan_SPuDSMessageBoxUpdate;
            _SysMan._SPuDSMarketUpdate += _SysMan__SPuDSMarketChartUpdate;
            _SysMan._Enable_AddSym += _SysMan__Enable_AddSym;
            _SysMan._PandLUpdate += _SysMan__setPandL;
            _SysMan._StateUpdate += _SysMan__setState;
            MainMessageBoxUpdateHandeler += MainMessageBoxUpdate;
            SPuDSMessageBoxUpdateHandeler += SPuDSMessageBoxUpdate;
            SPuDSMarketChartUpdateHandeler += SPuDSMarketChartUpdate;
            SPuDSPandLUpdateHandeler += setPandL;
            SPuDSStateUpdateHandeler += setState;
            Enable_AddSymHandeler += Enable_AddSym;
            comboBox_RemoveSymbol.Items.Add(" ");
            _SPuDS_Pages = new Dictionary<string, SPuDS_StratUI>();
        }
        ~SystemUI()
        {
            _SysMan.SPuDSClosePortfolio();
            _SysMan.Disconnect();
            _SysMan = null;
            GC.Collect();
        }

        private void button_IBConnection_Click(object sender, EventArgs e)
        {
            if (_IBdisconnected)
            {
                _SysMan.Connect();
                button_IBConnection.BackColor = Color.DarkRed;
                button_IBConnection.ForeColor = Color.White;
                button_IBConnection.Text = "Disconnect";
                _IBdisconnected = false;
            }
            else
            {
                _SysMan.Disconnect();
                button_IBConnection.BackColor = Color.LightGreen;
                button_IBConnection.ForeColor = Color.Black;
                button_IBConnection.Text = "Connect";
                _IBdisconnected = true;
            }
        }
        private void button_AddSym_Click(object sender, EventArgs e)
        {
            button_AddSym.Enabled = false;
            if (comboBox_AddSymbol.Text != "Enter Sym" && comboBox_AddSymbol.Items.Contains(comboBox_AddSymbol.Text))
            {
                String tempSymbol = comboBox_AddSymbol.Text.ToUpper();
                comboBox_AddSymbol.Items.Remove(tempSymbol);
                TabPage newTab = new TabPage(tempSymbol);
                SPuDS_StratUI newSPuDS_StratUI = new SPuDS_StratUI();

                tabControl_SPuDS.TabPages.Add(newTab);
                _SPuDS_Pages[tempSymbol] = newSPuDS_StratUI;

                newTab.Controls.Add(newSPuDS_StratUI);
                newTab.UseVisualStyleBackColor = true;
                newTab.BackColor = Color.SlateGray;

                newSPuDS_StratUI.Location = new System.Drawing.Point(0, 0);
                newSPuDS_StratUI.Name = "_SPuDS"+ tempSymbol;
                newSPuDS_StratUI.Size = new System.Drawing.Size(544, 306);
                newSPuDS_StratUI.TabIndex = 0;
                newSPuDS_StratUI.ParticipationRatesUpdated += SPuDS_StratUI_ParticipationRatesUpdated;
                comboBox_RemoveSymbol.Items.Add(tempSymbol);

                tabControl_SPuDS.SelectedTab = newTab;

                _SysMan.AddToSPuDS(tempSymbol);
                newSPuDS_StratUI.button_UpdateRate_Click(null, null);
                
            }
            else
            {
                MessageBox.Show("Invalid Symbol");
                button_AddSym.Enabled = true;
            }
            comboBox_AddSymbol.Text = "Enter Symbol";
            
        }
        private void SPuDS_StratUI_ParticipationRatesUpdated(double[] buy, double[] sell, int qty ,String stock)
        {
            _SysMan.setParticipationRates(buy, sell, qty, stock);
        }
        private void _SysMan__setPandL(double pandl, String stock)
        {
            this.BeginInvoke(SPuDSPandLUpdateHandeler, pandl, stock);
        }
        private void setPandL(double pandl, String stock)
        {
            _SPuDS_Pages[stock].set_PandL(pandl);
        }
        private void _SysMan__setState(String state, String stock)
        {
            this.BeginInvoke(SPuDSStateUpdateHandeler, state, stock);
        }
        private void setState(String state, String stock)
        {
            _SPuDS_Pages[stock].set_State(state);
        }
        private void button_RemoveSym_Click(object sender, EventArgs e)
        {
            if (comboBox_RemoveSymbol.SelectedText != " " )
            {
                TabPage tbToRemove = null;
                for (int i = 0; i < tabControl_SPuDS.TabPages.Count; i++)
                {
                    if (comboBox_RemoveSymbol.SelectedItem.ToString() == tabControl_SPuDS.TabPages[i].Text)
                    {
                        tbToRemove = tabControl_SPuDS.TabPages[i];
                        break;
                    }
                }
                _SysMan.RemoveFromSPuDS( tbToRemove.Text);
                tabControl_SPuDS.TabPages.Remove(tbToRemove);
                comboBox_AddSymbol.Items.Add(comboBox_RemoveSymbol.SelectedItem);
                comboBox_RemoveSymbol.Items.Remove(comboBox_RemoveSymbol.SelectedItem);
            }
        }
        private void comboBox_Enter(object sender, EventArgs e)
        {
            ComboBox tempComboBox = sender as ComboBox;

            if (tempComboBox.Text == "Enter Sym")
            {
                tempComboBox.Text = "";
            }
        }
        private void _SysMan__MainMessageBoxUpdate(String message)
        {
            this.BeginInvoke(MainMessageBoxUpdateHandeler, message);
        }
        private void MainMessageBoxUpdate(String message)
        {
            textBox_TWSMessages.Select(0, 0);
            textBox_TWSMessages.SelectedText = message + Environment.NewLine; 
        }
        private void _SysMan_SPuDSMessageBoxUpdate(String message, String stock)
        {
            this.BeginInvoke(SPuDSMessageBoxUpdateHandeler, message, stock);
        }
        private void SPuDSMessageBoxUpdate(String message, String stock)
        {
            _SPuDS_Pages[stock].addMessage(message);
        }
        private void _SysMan__SPuDSMarketChartUpdate(double price, string stock)
        {
            this.BeginInvoke(SPuDSMarketChartUpdateHandeler, price, stock);
        }
        private void SPuDSMarketChartUpdate(double price, String stock)
        {
            _SPuDS_Pages[stock].UpdateChart_Price(price);
        }
        private void _SysMan__Enable_AddSym()
        {
            this.BeginInvoke(Enable_AddSymHandeler);
        }
        private void Enable_AddSym()
        {
            button_AddSym.Enabled = true;
        }
    }
}
