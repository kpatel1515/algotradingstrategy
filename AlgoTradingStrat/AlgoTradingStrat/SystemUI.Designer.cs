﻿namespace AlgoTradingStrat
{
    partial class SystemUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_IBConnection = new System.Windows.Forms.Button();
            this.tabControl_SystemUI = new System.Windows.Forms.TabControl();
            this.tabPage_Main = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_PandL = new System.Windows.Forms.TextBox();
            this.textBox_TWSMessages = new System.Windows.Forms.TextBox();
            this.tabPage_SPuDS = new System.Windows.Forms.TabPage();
            this.comboBox_AddSymbol = new System.Windows.Forms.ComboBox();
            this.tabControl_SPuDS = new System.Windows.Forms.TabControl();
            this.comboBox_RemoveSymbol = new System.Windows.Forms.ComboBox();
            this.button_RemoveSym = new System.Windows.Forms.Button();
            this.button_AddSym = new System.Windows.Forms.Button();
            this.tabControl_SystemUI.SuspendLayout();
            this.tabPage_Main.SuspendLayout();
            this.tabPage_SPuDS.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_IBConnection
            // 
            this.button_IBConnection.BackColor = System.Drawing.Color.LightGreen;
            this.button_IBConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_IBConnection.Location = new System.Drawing.Point(6, 6);
            this.button_IBConnection.Name = "button_IBConnection";
            this.button_IBConnection.Size = new System.Drawing.Size(109, 71);
            this.button_IBConnection.TabIndex = 0;
            this.button_IBConnection.Text = "Connect";
            this.button_IBConnection.UseVisualStyleBackColor = false;
            this.button_IBConnection.Click += new System.EventHandler(this.button_IBConnection_Click);
            // 
            // tabControl_SystemUI
            // 
            this.tabControl_SystemUI.Controls.Add(this.tabPage_Main);
            this.tabControl_SystemUI.Controls.Add(this.tabPage_SPuDS);
            this.tabControl_SystemUI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl_SystemUI.Location = new System.Drawing.Point(2, 1);
            this.tabControl_SystemUI.Name = "tabControl_SystemUI";
            this.tabControl_SystemUI.SelectedIndex = 0;
            this.tabControl_SystemUI.Size = new System.Drawing.Size(783, 387);
            this.tabControl_SystemUI.TabIndex = 1;
            // 
            // tabPage_Main
            // 
            this.tabPage_Main.BackColor = System.Drawing.Color.Black;
            this.tabPage_Main.Controls.Add(this.label1);
            this.tabPage_Main.Controls.Add(this.textBox_PandL);
            this.tabPage_Main.Controls.Add(this.textBox_TWSMessages);
            this.tabPage_Main.Controls.Add(this.button_IBConnection);
            this.tabPage_Main.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Main.Name = "tabPage_Main";
            this.tabPage_Main.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Main.Size = new System.Drawing.Size(775, 361);
            this.tabPage_Main.TabIndex = 0;
            this.tabPage_Main.Text = "Main";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(573, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "Total P/L";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBox_PandL
            // 
            this.textBox_PandL.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBox_PandL.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.textBox_PandL.Location = new System.Drawing.Point(531, 25);
            this.textBox_PandL.Name = "textBox_PandL";
            this.textBox_PandL.Size = new System.Drawing.Size(145, 53);
            this.textBox_PandL.TabIndex = 2;
            this.textBox_PandL.Text = "P/L";
            // 
            // textBox_TWSMessages
            // 
            this.textBox_TWSMessages.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.textBox_TWSMessages.ForeColor = System.Drawing.SystemColors.Window;
            this.textBox_TWSMessages.Location = new System.Drawing.Point(7, 83);
            this.textBox_TWSMessages.Multiline = true;
            this.textBox_TWSMessages.Name = "textBox_TWSMessages";
            this.textBox_TWSMessages.Size = new System.Drawing.Size(762, 272);
            this.textBox_TWSMessages.TabIndex = 1;
            // 
            // tabPage_SPuDS
            // 
            this.tabPage_SPuDS.BackColor = System.Drawing.Color.Black;
            this.tabPage_SPuDS.Controls.Add(this.comboBox_AddSymbol);
            this.tabPage_SPuDS.Controls.Add(this.tabControl_SPuDS);
            this.tabPage_SPuDS.Controls.Add(this.comboBox_RemoveSymbol);
            this.tabPage_SPuDS.Controls.Add(this.button_RemoveSym);
            this.tabPage_SPuDS.Controls.Add(this.button_AddSym);
            this.tabPage_SPuDS.Location = new System.Drawing.Point(4, 22);
            this.tabPage_SPuDS.Name = "tabPage_SPuDS";
            this.tabPage_SPuDS.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_SPuDS.Size = new System.Drawing.Size(775, 361);
            this.tabPage_SPuDS.TabIndex = 1;
            this.tabPage_SPuDS.Text = "SPuDS";
            // 
            // comboBox_AddSymbol
            // 
            this.comboBox_AddSymbol.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox_AddSymbol.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox_AddSymbol.FormattingEnabled = true;
            this.comboBox_AddSymbol.Items.AddRange(new object[] {
            "AAPL",
            "AMZN",
            "CMCSA",
            "EBAY",
            "GOOGL"});
            this.comboBox_AddSymbol.Location = new System.Drawing.Point(6, 47);
            this.comboBox_AddSymbol.Name = "comboBox_AddSymbol";
            this.comboBox_AddSymbol.Size = new System.Drawing.Size(94, 21);
            this.comboBox_AddSymbol.TabIndex = 13;
            this.comboBox_AddSymbol.Text = "Enter Sym";
            this.comboBox_AddSymbol.Enter += new System.EventHandler(this.comboBox_Enter);
            // 
            // tabControl_SPuDS
            // 
            this.tabControl_SPuDS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl_SPuDS.Location = new System.Drawing.Point(207, 7);
            this.tabControl_SPuDS.Name = "tabControl_SPuDS";
            this.tabControl_SPuDS.SelectedIndex = 0;
            this.tabControl_SPuDS.Size = new System.Drawing.Size(555, 340);
            this.tabControl_SPuDS.TabIndex = 12;
            // 
            // comboBox_RemoveSymbol
            // 
            this.comboBox_RemoveSymbol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_RemoveSymbol.FormattingEnabled = true;
            this.comboBox_RemoveSymbol.Location = new System.Drawing.Point(106, 47);
            this.comboBox_RemoveSymbol.Name = "comboBox_RemoveSymbol";
            this.comboBox_RemoveSymbol.Size = new System.Drawing.Size(94, 21);
            this.comboBox_RemoveSymbol.TabIndex = 11;
            // 
            // button_RemoveSym
            // 
            this.button_RemoveSym.Location = new System.Drawing.Point(106, 6);
            this.button_RemoveSym.Name = "button_RemoveSym";
            this.button_RemoveSym.Size = new System.Drawing.Size(94, 37);
            this.button_RemoveSym.TabIndex = 10;
            this.button_RemoveSym.Text = "Remove Stock";
            this.button_RemoveSym.UseVisualStyleBackColor = true;
            this.button_RemoveSym.Click += new System.EventHandler(this.button_RemoveSym_Click);
            // 
            // button_AddSym
            // 
            this.button_AddSym.Location = new System.Drawing.Point(6, 6);
            this.button_AddSym.Name = "button_AddSym";
            this.button_AddSym.Size = new System.Drawing.Size(94, 37);
            this.button_AddSym.TabIndex = 9;
            this.button_AddSym.Text = "Add Stock";
            this.button_AddSym.UseVisualStyleBackColor = true;
            this.button_AddSym.Click += new System.EventHandler(this.button_AddSym_Click);
            // 
            // SystemUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(797, 400);
            this.Controls.Add(this.tabControl_SystemUI);
            this.Name = "SystemUI";
            this.Text = "Form1";
            this.tabControl_SystemUI.ResumeLayout(false);
            this.tabPage_Main.ResumeLayout(false);
            this.tabPage_Main.PerformLayout();
            this.tabPage_SPuDS.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_IBConnection;
        private System.Windows.Forms.TabControl tabControl_SystemUI;
        private System.Windows.Forms.TabPage tabPage_Main;
        private System.Windows.Forms.TextBox textBox_TWSMessages;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_PandL;
        private System.Windows.Forms.TabPage tabPage_SPuDS;
        private System.Windows.Forms.ComboBox comboBox_RemoveSymbol;
        private System.Windows.Forms.Button button_RemoveSym;
        private System.Windows.Forms.Button button_AddSym;
        private System.Windows.Forms.TabControl tabControl_SPuDS;
        private System.Windows.Forms.ComboBox comboBox_AddSymbol;
    }
}

