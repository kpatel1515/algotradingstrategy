﻿namespace AlgoTradingStrat
{
    partial class SPuDS_StratUI
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chart_PriceHist = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.textBox_PandL = new System.Windows.Forms.TextBox();
            this.label_PandL = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_currentState = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_BDown = new System.Windows.Forms.TextBox();
            this.textBox_BUpNeutral = new System.Windows.Forms.TextBox();
            this.textBox_BDownNeutral = new System.Windows.Forms.TextBox();
            this.textBox_BUp = new System.Windows.Forms.TextBox();
            this.textBox_SUp = new System.Windows.Forms.TextBox();
            this.textBox_SUpNeutral = new System.Windows.Forms.TextBox();
            this.textBox_SDownNeutral = new System.Windows.Forms.TextBox();
            this.textBox_SDown = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.numericUpDown_Qty = new System.Windows.Forms.NumericUpDown();
            this.textBox_Messages = new System.Windows.Forms.TextBox();
            this.button_UpdateRate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.chart_PriceHist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Qty)).BeginInit();
            this.SuspendLayout();
            // 
            // chart_PriceHist
            // 
            chartArea2.Name = "ChartArea1";
            this.chart_PriceHist.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart_PriceHist.Legends.Add(legend2);
            this.chart_PriceHist.Location = new System.Drawing.Point(3, 3);
            this.chart_PriceHist.Name = "chart_PriceHist";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.IsVisibleInLegend = false;
            series2.LabelAngle = 90;
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chart_PriceHist.Series.Add(series2);
            this.chart_PriceHist.Size = new System.Drawing.Size(343, 164);
            this.chart_PriceHist.SuppressExceptions = true;
            this.chart_PriceHist.TabIndex = 0;
            this.chart_PriceHist.Text = "chart_PriceHist";
            // 
            // textBox_PandL
            // 
            this.textBox_PandL.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_PandL.Location = new System.Drawing.Point(210, 186);
            this.textBox_PandL.Name = "textBox_PandL";
            this.textBox_PandL.ReadOnly = true;
            this.textBox_PandL.Size = new System.Drawing.Size(100, 26);
            this.textBox_PandL.TabIndex = 1;
            this.textBox_PandL.Text = "0.00";
            this.textBox_PandL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label_PandL
            // 
            this.label_PandL.AutoSize = true;
            this.label_PandL.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_PandL.Location = new System.Drawing.Point(218, 170);
            this.label_PandL.Name = "label_PandL";
            this.label_PandL.Size = new System.Drawing.Size(93, 16);
            this.label_PandL.TabIndex = 2;
            this.label_PandL.Text = "Strategy P/L";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(418, 167);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Max Qty";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(43, 169);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "Current State";
            // 
            // textBox_currentState
            // 
            this.textBox_currentState.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_currentState.Location = new System.Drawing.Point(26, 186);
            this.textBox_currentState.Name = "textBox_currentState";
            this.textBox_currentState.ReadOnly = true;
            this.textBox_currentState.Size = new System.Drawing.Size(137, 26);
            this.textBox_currentState.TabIndex = 5;
            this.textBox_currentState.Text = "DownNeutral";
            this.textBox_currentState.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(393, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Participation Rate";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBox_BDown
            // 
            this.textBox_BDown.Location = new System.Drawing.Point(370, 137);
            this.textBox_BDown.Name = "textBox_BDown";
            this.textBox_BDown.Size = new System.Drawing.Size(45, 20);
            this.textBox_BDown.TabIndex = 11;
            this.textBox_BDown.Tag = "3";
            this.textBox_BDown.Text = "0.0";
            this.textBox_BDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_BUpNeutral
            // 
            this.textBox_BUpNeutral.Location = new System.Drawing.Point(370, 75);
            this.textBox_BUpNeutral.Name = "textBox_BUpNeutral";
            this.textBox_BUpNeutral.Size = new System.Drawing.Size(45, 20);
            this.textBox_BUpNeutral.TabIndex = 13;
            this.textBox_BUpNeutral.Tag = "1";
            this.textBox_BUpNeutral.Text = "75.0";
            this.textBox_BUpNeutral.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_BDownNeutral
            // 
            this.textBox_BDownNeutral.Location = new System.Drawing.Point(370, 106);
            this.textBox_BDownNeutral.Name = "textBox_BDownNeutral";
            this.textBox_BDownNeutral.Size = new System.Drawing.Size(45, 20);
            this.textBox_BDownNeutral.TabIndex = 12;
            this.textBox_BDownNeutral.Tag = "2";
            this.textBox_BDownNeutral.Text = "25.0";
            this.textBox_BDownNeutral.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_BUp
            // 
            this.textBox_BUp.Location = new System.Drawing.Point(370, 44);
            this.textBox_BUp.Name = "textBox_BUp";
            this.textBox_BUp.Size = new System.Drawing.Size(45, 20);
            this.textBox_BUp.TabIndex = 14;
            this.textBox_BUp.Tag = "0";
            this.textBox_BUp.Text = "100.0";
            this.textBox_BUp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_SUp
            // 
            this.textBox_SUp.Location = new System.Drawing.Point(488, 48);
            this.textBox_SUp.Name = "textBox_SUp";
            this.textBox_SUp.Size = new System.Drawing.Size(45, 20);
            this.textBox_SUp.TabIndex = 21;
            this.textBox_SUp.Tag = "0";
            this.textBox_SUp.Text = "0.0";
            this.textBox_SUp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_SUpNeutral
            // 
            this.textBox_SUpNeutral.Location = new System.Drawing.Point(488, 78);
            this.textBox_SUpNeutral.Name = "textBox_SUpNeutral";
            this.textBox_SUpNeutral.Size = new System.Drawing.Size(45, 20);
            this.textBox_SUpNeutral.TabIndex = 20;
            this.textBox_SUpNeutral.Tag = "1";
            this.textBox_SUpNeutral.Text = "25.0";
            this.textBox_SUpNeutral.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_SDownNeutral
            // 
            this.textBox_SDownNeutral.Location = new System.Drawing.Point(488, 108);
            this.textBox_SDownNeutral.Name = "textBox_SDownNeutral";
            this.textBox_SDownNeutral.Size = new System.Drawing.Size(45, 20);
            this.textBox_SDownNeutral.TabIndex = 19;
            this.textBox_SDownNeutral.Tag = "2";
            this.textBox_SDownNeutral.Text = "75.0";
            this.textBox_SDownNeutral.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_SDown
            // 
            this.textBox_SDown.Location = new System.Drawing.Point(488, 138);
            this.textBox_SDown.Name = "textBox_SDown";
            this.textBox_SDown.Size = new System.Drawing.Size(45, 20);
            this.textBox_SDown.TabIndex = 18;
            this.textBox_SDown.Tag = "3";
            this.textBox_SDown.Text = "100.0";
            this.textBox_SDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(441, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Up";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(424, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "UpNeutral";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(417, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "DownNeutral";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(434, 141);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Down";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(380, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(25, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "Buy";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(498, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(24, 13);
            this.label12.TabIndex = 30;
            this.label12.Text = "Sell";
            // 
            // numericUpDown_Qty
            // 
            this.numericUpDown_Qty.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_Qty.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown_Qty.Location = new System.Drawing.Point(396, 186);
            this.numericUpDown_Qty.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDown_Qty.Name = "numericUpDown_Qty";
            this.numericUpDown_Qty.Size = new System.Drawing.Size(91, 26);
            this.numericUpDown_Qty.TabIndex = 31;
            this.numericUpDown_Qty.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDown_Qty.Enter += new System.EventHandler(this.Text_Selection);
            this.numericUpDown_Qty.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Text_Selection);
            // 
            // textBox_Messages
            // 
            this.textBox_Messages.Location = new System.Drawing.Point(4, 223);
            this.textBox_Messages.Multiline = true;
            this.textBox_Messages.Name = "textBox_Messages";
            this.textBox_Messages.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_Messages.Size = new System.Drawing.Size(537, 83);
            this.textBox_Messages.TabIndex = 32;
            this.textBox_Messages.WordWrap = false;
            // 
            // button_UpdateRate
            // 
            this.button_UpdateRate.Location = new System.Drawing.Point(421, 23);
            this.button_UpdateRate.Name = "button_UpdateRate";
            this.button_UpdateRate.Size = new System.Drawing.Size(55, 23);
            this.button_UpdateRate.TabIndex = 33;
            this.button_UpdateRate.Text = "Update";
            this.button_UpdateRate.UseVisualStyleBackColor = true;
            this.button_UpdateRate.Click += new System.EventHandler(this.button_UpdateRate_Click);
            // 
            // SPuDS_StratUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button_UpdateRate);
            this.Controls.Add(this.textBox_Messages);
            this.Controls.Add(this.numericUpDown_Qty);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox_SUp);
            this.Controls.Add(this.textBox_SUpNeutral);
            this.Controls.Add(this.textBox_SDownNeutral);
            this.Controls.Add(this.textBox_SDown);
            this.Controls.Add(this.textBox_BUp);
            this.Controls.Add(this.textBox_BUpNeutral);
            this.Controls.Add(this.textBox_BDownNeutral);
            this.Controls.Add(this.textBox_BDown);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox_currentState);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_PandL);
            this.Controls.Add(this.textBox_PandL);
            this.Controls.Add(this.chart_PriceHist);
            this.Name = "SPuDS_StratUI";
            this.Size = new System.Drawing.Size(544, 306);
            ((System.ComponentModel.ISupportInitialize)(this.chart_PriceHist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Qty)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart_PriceHist;
        private System.Windows.Forms.TextBox textBox_PandL;
        private System.Windows.Forms.Label label_PandL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_currentState;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_BDown;
        private System.Windows.Forms.TextBox textBox_BUpNeutral;
        private System.Windows.Forms.TextBox textBox_BDownNeutral;
        private System.Windows.Forms.TextBox textBox_BUp;
        private System.Windows.Forms.TextBox textBox_SUp;
        private System.Windows.Forms.TextBox textBox_SUpNeutral;
        private System.Windows.Forms.TextBox textBox_SDownNeutral;
        private System.Windows.Forms.TextBox textBox_SDown;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown numericUpDown_Qty;
        private System.Windows.Forms.TextBox textBox_Messages;
        private System.Windows.Forms.Button button_UpdateRate;
    }
}
