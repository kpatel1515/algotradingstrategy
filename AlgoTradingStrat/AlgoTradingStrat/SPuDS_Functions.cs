﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AlgoTradingStrat
{
    public partial class SPuDS_Strategy
    {
        /************* indicator functions *****************/

        private void Fun_MACD(double Short, double Long, double dea, ref double[] prices, 
                                out double[] DIF, out double[] DEA, out double EMAl, out double EMAs)
        {    // length MACD =length prices   
            //denominaors and numerators
            int n = prices.Length;
            double Short_a = 2.0 / (Short + 1);
            double Short_b = 1.0 - Short_a;
            double Long_a = 2.0 / (Long + 1);
            double Long_b = 1.0 - Long_a;
            double dea_a = 2.0 / (dea + 1);
            double dea_b = 1.0 - dea_a;
            EMAl = prices[1];
            EMAs = prices[1];

            DEA = new double[n];
            DIF = new double[n];
            DEA[0] = EMAs - EMAl;
            DIF[0] = EMAs - EMAl;

            for (int i = 1; i < n; ++i)
            {
                EMAs = prices[i] * Short_a + EMAs * Short_b;
                EMAl = prices[i] * Long_a + EMAl * Long_b;
                DIF[i] = EMAs - EMAl;
                DEA[i] = DIF[i] * dea_a + DEA[i - 1] * dea_b;
            }
        }

        private void Update_MACD(double Short, double Long, double dea, double price, double DEAOld, double EMAsOld, double EMAlOld, 
                                 out double DEA, out double EMAsNew, out double EMAlNew)
        {
            double Short_a = 2.0 / (Short + 1);
            double Short_b = 1.0 - Short_a;
            double Long_a = 2.0 / (Long + 1);
            double Long_b = 1.0 - Long_a;
            double dea_a = 2.0 / (dea + 1);
            double dea_b = 1.0 - dea_a;

            EMAsNew = price * Short_a + EMAsOld * Short_b;
            EMAlNew = price * Long_a + EMAlOld * Long_b;
            //DIF = EMAsNew - EMAlNew;
            //DEA = DIF*dea_a + DEAOld*dea_b;
            DEA = (EMAsNew - EMAlNew) * dea_a + DEAOld * dea_b;
        }

        private void Fun_RSI(int period, ref double[] prices, out double[] RSI, out double mean_Gain, out double mean_Loss)
        {
            int n = prices.Length;
            RSI = new double[n];
            double[] PandL = new double[n];
            mean_Gain = 0;
            mean_Loss = 0;
            int num_Gain = 0;
            int num_Loss = 0;

            PandL[0] = 0;
            RSI[0] = 0;
            for (int i = 1; i < period; ++i)
            {
                PandL[i] = prices[i] - prices[i - 1];
                if (PandL[i] >= 0)
                {
                    num_Gain += 1;
                    mean_Gain += PandL[i];
                }
                else
                {
                    num_Loss += 1;
                    mean_Loss += PandL[i];
                }
                RSI[i] = 0;
            }
            mean_Gain = mean_Gain / num_Gain;
            mean_Loss = -mean_Loss / num_Loss;
            RSI[period] = 100 * mean_Gain / (mean_Gain + mean_Loss);

            double na = 2.0 / (period + 1);
            double nb = 1.0 - na;
            for (int i = period; i < n; ++i)
            {
                PandL[i] = prices[i] - prices[i - 1];
                mean_Gain = (PandL[i] > 0 ? PandL[i] : 0) * na + mean_Gain * nb;
                mean_Loss = -(PandL[i] < 0 ? PandL[i] : 0) * na + mean_Loss * nb;
                RSI[i] = 100 * mean_Gain / (mean_Gain + mean_Loss);
            }
        }

        private void Update_RSI(int period, double[] prices, double mean_GainOld, double mean_LossOld,
                                out double RSI, out double mean_GainNew, out double mean_LossNew)
        {
            double na = 2.0 / (period + 1);
            double nb = 1.0 - na;
            double PandL = prices[1] - prices[0];
            mean_GainNew = (PandL > 0 ? PandL : 0) * na + mean_GainOld * nb;
            mean_LossNew = -(PandL < 0 ? PandL : 0) * na + mean_LossOld * nb;
            RSI = 100 * mean_GainNew / (mean_GainNew + mean_LossNew); 
        }

        private void Fun_FI(int period, ref double[] prices, ref double[] volumes, out double[] FI)
        {
            double na = 2.0 / (period + 1);
            double nb = 1.0 - na;
            int n = prices.Length;
            FI = new double[n];
            FI[0] = 0;

            for (int i = 1; i < n; ++i)
            {
                FI[i] = na * (prices[i] - prices[i - 1]) * volumes[i] + nb * FI[i - 1];
            }
        }

        // zigzag function for response
        private void zigzag(double threshold, ref double[] prices, out List<int> indices, out List<double> slopes)
        {
            int n = prices.Length;
            int start_index = 0;
            indices = new List<int>();
            slopes = new List<double>();

            while (start_index < n)
            {
                int current_index = start_index;
                for (current_index = start_index; current_index < n - 1; ++current_index)
                {
                    if (Math.Abs((prices[current_index] - prices[start_index]) / prices[start_index]) >= threshold)
                    {
                        if (Math.Abs(prices[current_index + 1] - prices[start_index]) < Math.Abs(prices[current_index] - prices[start_index]))
                        {
                            indices.Add(current_index);
                            slopes.Add((prices[current_index] - prices[start_index]) / prices[start_index]);
                            start_index = current_index;
                            break;
                        }
                    }
                }
                // deal with end of the array
                if (current_index == n-1)
                {
                    indices.Add(current_index );
                    slopes.Add((prices[current_index ] - prices[start_index]) / prices[start_index]);
                    break;
                }
                
                else if (current_index == n - 2 && current_index == start_index)
                {
                    if (Math.Abs(prices[n - 1] - prices[n - 2]) <= 0.0001) // last two price very close
                    {
                        indices[indices.Count] = n - 1; //replace the existing last index, n-2 to n-1
                        slopes[slopes.Count] = (prices[n - 1] - prices[indices[indices.Count - 1]]) / prices[indices[indices.Count - 1]];
                        break;
                    }
                    else   // last two price has a non-negligible difference
                    {
                        indices.Add(n - 1);
                        slopes.Add((prices[n - 1] - prices[n - 2]) / prices[n - 2]);
                        break;
                    }
                }
            }
        }

        // quantile function compute a single quantile
        private double quantile(double quant, ref double[] values)
        {
            if (quant > 1)
            {
                quant = 1;
            }
            else if (quant < 0)
            {
                quant = 0;
            }
            int num = values.Length;
            int q = (int)(Math.Ceiling(num * quant));
            return values[q - 1];
        }

        // discretize a single input or one data point, for trading
        // order of the indicators RSI1,RSI2,RSI3,MACD_DIF,MACD_DEA,FI
        // order of the discrete output  RSI1,RSI2,RSI3,MACD,FI

        private void discret_input(double[] indicators, out double[] discrete)
        {
            discrete = new double[4];

            if (indicators[0] <= 20) discrete[0] = 0;
            else if (indicators[0] > 20 && indicators[0] <= 40) discrete[0] = 1;
            else if (indicators[0] > 40 && indicators[0] <= 60) discrete[0] = 2;
            else if (indicators[0] > 60 && indicators[0] <= 80) discrete[0] = 3;
            else discrete[0] = 4;

            if (indicators[1] <= 20) discrete[1] = 0;
            else if (indicators[1] > 20 && indicators[1] <= 40) discrete[1] = 1;
            else if (indicators[1] > 40 && indicators[1] <= 60) discrete[1] = 2;
            else if (indicators[1] > 60 && indicators[1] <= 80) discrete[1] = 3;
            else discrete[1] = 4;

            if (indicators[2] <= 20) discrete[2] = 0;
            else if (indicators[2] > 20 && indicators[2] <= 40) discrete[2] = 1;
            else if (indicators[2] > 40 && indicators[2] <= 60) discrete[2] = 2;
            else if (indicators[2] > 60 && indicators[2] <= 80) discrete[2] = 3;
            else discrete[2] = 4;

            if (indicators[3]-indicators[4] > 0 & indicators[3] < 0 & indicators[4] < 0) discrete[3] = 0;
            else if (indicators[3]-indicators[4] < 0 & indicators[3] > 0 & indicators[4] > 0) discrete[3] = 1;
            else discrete[3] = 2;
        }
    }
}