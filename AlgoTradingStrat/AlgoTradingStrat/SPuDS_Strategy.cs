﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AlgoTradingStrat
{
    public enum MarketState
    {
        UP = 1,
        UpNEUTRAL = 2,
        DOWN = 3,
        DownNEUTRAL = 4
    }
    public struct TradeDump // format of data before processing
    {
        //time,price,quantity,board,source,buyer,seller,initiator ==> TradeDump
        public String Time {get; set;}
        public double Price {get; set;}
        public int Quantity {get; set;}
    }

    public partial class SPuDS_Strategy
    {
        private const int _NumOfIndicators = 1;
        public int NumOfIndicators
        {
            get { return _NumOfIndicators; }
        }
        
        private const int _NumOfStates = 4;
        public int NumOfStates
        {
            get { return _NumOfStates; }
        }
        
        private MarketState _currentmarketstate;
        public MarketState CurrentMarketState
        {
            get { return _currentmarketstate; }
        }

        private Double _pandl = 0.0;
        public Double getPandL
        {
            get { return _pandl; }
        }

        private Double _lastprice = 0.0;
        
        
        public double[] Buy_ParticipationRate { get; set; }
        public double[] Sell_ParticipationRate { get; set; }
        public int Max_Qty{get; set;}
        
        public SPuDS_Strategy() // constructor 
        {
            Buy_ParticipationRate = new double[_NumOfStates];
            Sell_ParticipationRate = new double[_NumOfStates];
            _currentmarketstate = new MarketState();
            _currentmarketstate = MarketState.DownNEUTRAL;
        }

        private int current_position = 0;
        public int CurrentPosition
        {
            get { return current_position; }
        }
        private Double newPrice = 0;
        private int newQty = 0;

        // Intermediate variables for update indicators
        private double EMAlOld=new double(); //DIF=EMAs - EMAl
        private double EMAsOld=new double();
        private double DEAOld = new double();
        private double mean_GainOld1 = new double();
        private double mean_LossOld1 = new double();
        private double mean_GainOld2 = new double();
        private double mean_LossOld2 = new double();
        private double mean_GainOld3 = new double();
        private double mean_LossOld3 = new double();
        private double price_old = new double();
        
        // the classifier
        private alglib.decisionforest RF_Classifier;

        public bool Update(int Qty)
        {
            MarketState lastState = _currentmarketstate;
            newQty = Qty;
            if (newPrice != 0)
            {
                double[] trading_data = { newPrice, newQty };
                int predicted; ;
                UpdateStrategy(trading_data, RF_Classifier, out predicted);

                switch (lastState)
                {
                    case MarketState.UP:
                        if (predicted == 1) { _currentmarketstate = MarketState.UpNEUTRAL; break; }
                        else if (predicted == 0) { _currentmarketstate = MarketState.DOWN; break; }
                        break;
                    case MarketState.UpNEUTRAL:
                        if (predicted == 1) { _currentmarketstate = MarketState.UpNEUTRAL; break; }
                        else if (predicted == 0) { _currentmarketstate = MarketState.DOWN; break; }
                        break;
                    case MarketState.DOWN:
                        if (predicted == 1) { _currentmarketstate = MarketState.UP; break; }
                        else if (predicted == 0) { _currentmarketstate = MarketState.DownNEUTRAL; break; }
                        break;
                    case MarketState.DownNEUTRAL:
                        if (predicted == 1) { _currentmarketstate = MarketState.UP; break; }
                        else if (predicted == 0) { _currentmarketstate = MarketState.DownNEUTRAL; break; }
                        break;
                }
                newQty = 0;
                newPrice = 0;
            }

            if (_currentmarketstate == lastState)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool Update(double Price)
        {
            MarketState lastState = _currentmarketstate;
            newPrice = Price;
            if (_lastprice == 0.0)
            {
                _lastprice = newPrice;
            }
            else
            {
                _pandl += current_position * (newPrice - _lastprice);
            }

            if (newQty != 0)
            {
                double[] trading_data = {newPrice, newQty};
                int predicted;               
                UpdateStrategy (trading_data, RF_Classifier, out predicted);

                switch (lastState)
                {
                    case MarketState.UP:
                        if (predicted == 1) { _currentmarketstate = MarketState.UpNEUTRAL; break; }
                        else if (predicted == 0) { _currentmarketstate = MarketState.DOWN; break; }
                        break;
                    case MarketState.UpNEUTRAL:
                        if (predicted == 1) { _currentmarketstate = MarketState.UpNEUTRAL; break; }
                        else if (predicted == 0) { _currentmarketstate = MarketState.DOWN; break; }
                        break;
                    case MarketState.DOWN:
                        if (predicted == 1) { _currentmarketstate = MarketState.UP; break; }
                        else if (predicted == 0) { _currentmarketstate = MarketState.DownNEUTRAL; break; }
                        break;
                    case MarketState.DownNEUTRAL:
                        if (predicted == 1) { _currentmarketstate = MarketState.UP; break; }
                        else if (predicted == 0) { _currentmarketstate = MarketState.DownNEUTRAL; break; }
                        break;
                }
                newQty = 0;
                newPrice = 0;
            }

            if (_currentmarketstate == lastState)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public int getQtyDiff()
        {
            int QtyToTrade = 0;
            if (_currentmarketstate == MarketState.UP)
            {
                QtyToTrade = Convert.ToInt32(Max_Qty * (Buy_ParticipationRate[0] - Sell_ParticipationRate[0])- current_position);
            }
            else if (_currentmarketstate == MarketState.UpNEUTRAL)
            {
                QtyToTrade = Convert.ToInt32(Max_Qty * (Buy_ParticipationRate[1] - Sell_ParticipationRate[1]) - current_position);
            }
            else if (_currentmarketstate == MarketState.DownNEUTRAL)
            {
                QtyToTrade = Convert.ToInt32(Max_Qty * (Buy_ParticipationRate[2] - Sell_ParticipationRate[2]) - current_position);
            }
            else
            {
                QtyToTrade = Convert.ToInt32(Max_Qty * (Buy_ParticipationRate[3] - Sell_ParticipationRate[3]) - current_position);
            }

            current_position += QtyToTrade;
            return QtyToTrade;
        }
        
        // training data processing, classifier taining and updates 
        public void ProcessHistoricalData(List<TradeDump> tradeData)
        {            
            //int length = tradeData.Count;
            int length = 2000;
            //string[] time = new string[length];
            double[] price =new double[length];
            double[] volume = new double[length];           
           
            int i=0;
            foreach (TradeDump trade in tradeData)
            {
                //time[i] = trade.Time;
                price[i] = trade.Price;                
                volume[i] = trade.Quantity;                
                if (i >= length-1) break;
                i++;
            }

            // costruct indicators
            //double[] MACDc; direct compute by DIF-DEA
            double[] DIFc;
            double[] DEAc;
            double[] RSI1c;
            double[] RSI2c;
            double[] RSI3c;
            double[] FIc;
            //string[] hour; // don't need any more    
       
            Fun_MACD(3, 7, 5, ref price, out DIFc, out DEAc, out EMAlOld, out EMAsOld);
            Fun_RSI(3, ref price, out RSI1c, out mean_GainOld1, out mean_LossOld1);
            Fun_RSI(5, ref price, out RSI2c, out mean_GainOld2, out mean_LossOld2);
            Fun_RSI(7, ref price, out RSI3c, out mean_GainOld3, out mean_LossOld3);
            Fun_FI(5, ref price, ref volume, out FIc);

            // update intermediate variables
            DEAOld=DEAc[DEAc.Length-1];
            price_old = price[price.Length - 1];

            List<int> indices = new List<int>();
            List<double> slopes = new List<double>();
            double threshold = 0.01;
            zigzag(threshold, ref price, out indices, out slopes);
            indices.ToArray();
            slopes.ToArray();

            // discretization and mismatch          
            // traning data is filled backwardly
            // order of predictors RSI1,RSI2,RSI3,MACD,FI
            int drop = 10;
            int mis_period = 1;
            int length_drop = length - drop;
            double[,] proData = new double[length-drop-mis_period,5];// used to contain the "discrete" results

            int index = indices.Count - 1;
            for (int j = 1; j <= length-drop-mis_period; ++j)
            {
                // response discretization, only applied when training
               
                for (; index > 0;--index )
                {
                    if (length - j >= indices[index])
                    {
                        break;
                    }
                }

                if (slopes[index] > 0) proData[length - drop - mis_period - j,4] = 0;
                else proData[length - drop - mis_period - j, 4] = 1;
               

                // predictors discritization
                if (RSI1c[length - mis_period - j] <= 20) proData[length - mis_period - drop - j,0] = 0;
                else if (RSI1c[length - mis_period - j] > 20 && RSI1c[length - mis_period - j] <= 40) proData[length - mis_period - drop - j,0] = 1;
                else if (RSI1c[length - mis_period - j] > 40 && RSI1c[length - mis_period - j] <= 60) proData[length - mis_period - drop - j,0] = 2;
                else if (RSI1c[length - mis_period - j] > 60 && RSI1c[length - mis_period - j] <= 80) proData[length - mis_period - drop - j,0] = 3;
                else proData[length - mis_period - drop - j,0] = 4;

                if (RSI2c[length - mis_period - j] <= 20) proData[length - mis_period - drop - j,1] = 0;
                else if (RSI2c[length - mis_period - j] > 20 && RSI2c[length - mis_period - j] <= 40) proData[length - mis_period - drop - j, 1] = 1;
                else if (RSI2c[length - mis_period - j] > 40 && RSI2c[length - mis_period - j] <= 60) proData[length - mis_period - drop - j, 1] = 2;
                else if (RSI2c[length - mis_period - j] > 60 && RSI2c[length - mis_period - j] <= 80) proData[length - mis_period - drop - j, 1] = 3;
                else proData[length - mis_period - drop - j, 1] = 4;

                if (RSI3c[length - mis_period - j] <= 20) proData[length - mis_period - drop - j,2] = 0;
                else if (RSI3c[length - mis_period - j] > 20 && RSI3c[length - mis_period - j] <= 40) proData[length - mis_period - drop - j, 2] = 1;
                else if (RSI3c[length - mis_period - j] > 40 && RSI3c[length - mis_period - j] <= 60) proData[length - mis_period - drop - j, 2] = 2;
                else if (RSI3c[length - mis_period - j] > 60 && RSI3c[length - mis_period - j] <= 80) proData[length - mis_period - drop - j, 2] = 3;
                else proData[length - mis_period - drop - j, 2] = 4;

                if (DIFc[length - mis_period - j] - DEAc[length - mis_period - j] > 0 & DIFc[length - mis_period - j] < 0 & DEAc[length - mis_period - j] < 0) proData[length - mis_period - drop - j,3] = 0;
                else if (DIFc[length - mis_period - j] - DEAc[length - mis_period - j] < 0 & DIFc[length - mis_period - j] > 0 & DEAc[length - mis_period - j] > 0) proData[length - mis_period - drop - j, 3] = 1;
                else proData[length - mis_period - drop - j, 3] = 2;

                //double FI1 = quantile(0.2,ref FIc);
                //double FI2 = quantile(0.4,ref FIc);
                //double FI3 = quantile(0.6,ref FIc);
                //double FI4 = quantile(0.8,ref FIc);
                //if (FIc[length - mis_period - j] <= FI1) proData[length - mis_period - drop - j,5] = 0;
                //else if (FIc[length - mis_period - j] > FI1 && FIc[length - mis_period - j] <= FI2) proData[length - mis_period - drop - j, 5] = 1;
                //else if (FIc[length - mis_period - j] > FI2 && FIc[length - mis_period - j] <= FI3) proData[length - mis_period - drop - j, 5] = 2;
                //else if (FIc[length - mis_period - j] > FI3 && FIc[length - mis_period - j] <= FI4) proData[length - mis_period - drop - j, 5] = 3;
                //else proData[length - mis_period - drop - j, 5] = 4;
            }
            // train and update RF classifier
           
            TrainStartegy(proData, out RF_Classifier);        
        }
        private void TrainStartegy( double [,] training_data, out alglib.decisionforest df )
        {
            int input_num = training_data.GetLength(1);
            int  data_num = training_data.GetLength(0);
            int info;
            df=new alglib.decisionforest();
            alglib.dfreport dfrep;   
            alglib.dfbuildrandomdecisionforest(training_data, data_num, input_num - 1, 2, 100, 0.6, out info, out df, out dfrep);

            // for test
            if (info == -2)
            {
                int i=0;
            }
            if( info ==-1)
            {
                int i = 0;
            }

        }

        private void UpdateStrategy( double[] trading_data, alglib.decisionforest forecast, out int predicted)
        {           
            // Intermediate variables for update indicators
            double EMAlNew=new double(); //DIF=EMAs - EMAl
            double EMAsNew=new double();
            double DEANew = new double();
            double mean_GainNew1 = new double();
            double mean_LossNew1 = new double();
            double mean_GainNew2 = new double();
            double mean_LossNew2 = new double();
            double mean_GainNew3 = new double();
            double mean_LossNew3 = new double();
            double RSI1=new double();
            double RSI2=new double();
            double RSI3=new double();


            // update indicators  

            //public static void alglib.dfprocess(decisionforest df, double[] x, ref double[] y)
            Update_MACD(3, 7, 5, trading_data[0], DEAOld, EMAlOld, EMAlOld, out DEANew , out EMAsNew, out EMAlNew);

            double[] Price_update = { price_old, trading_data[0] };
            
            Update_RSI(3, Price_update, mean_GainOld1, mean_LossOld1, out RSI1, out mean_GainNew1, out mean_LossNew1);
            Update_RSI(5, Price_update, mean_GainOld2, mean_LossOld2, out RSI2, out mean_GainNew2, out mean_LossNew2);
            Update_RSI(7, Price_update, mean_GainOld3, mean_LossOld3, out RSI3, out mean_GainNew3, out mean_LossNew3);
            
            //update old intermedia values
            price_old = trading_data[0];
            EMAlOld = EMAlNew; //DIF=EMAs - EMAl
            EMAsOld = EMAsNew;
            mean_GainOld1 = mean_GainNew1;
            mean_GainOld2 = mean_GainNew2;
            mean_GainOld3 = mean_GainNew3;
            mean_LossOld1 = mean_LossNew1;
            mean_LossOld2 = mean_LossNew2;
            mean_LossOld3 = mean_LossNew3;            

            // order of the indicators RSI1,RSI2,RSI3,MACD_DIF,MACD_DEA,//FI
            // order of the discrete output  RSI1,RSI2,RSI3,MACD,//FI
            double [] indicators={RSI1,RSI2,RSI3,EMAsNew-EMAlNew,DEANew};
            double[] trading_discrete = new double[4];
            discret_input(indicators, out trading_discrete);            
            
            // predict trading resultud
            double[] predict = new double[2]; //posterior probabilities

            try
            {
                alglib.dfprocess(forecast, trading_discrete, ref predict);
            }
            catch (Exception e)
            {
                int test = 0;
            }

            if (predict[0] > predict[1])
            {
                predicted = 0;
            }
            else 
            {
                predicted = 1;
            }
        }
    }//end of class           
}


