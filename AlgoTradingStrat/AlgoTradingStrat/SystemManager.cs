﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Net;
using System.Threading.Tasks;
using System.IO;
using TWSLib;

namespace AlgoTradingStrat
{
    public delegate void IDMessageUpdate( IDMessage idM);
    public delegate void IDMarketUpdate( IDMarketParam idP);
    public delegate void EnableButton();
    
    public class IDMessage
    {
        private int id;
        public int ID
        {
            get { return id; }
            set { id = value;} 
        }
        private String message;
        public String Message
        {
            get {  return message;}
            set { message = value; }
        }
        public IDMessage(int num, String text)
        {
            id = num;
            message = String.Copy(text);
        }
    }
    public class IDMarketParam
    {
        private int id;
        public int ID
        {
            get { return id; }
            set { id = value;} 
        }
        private double price;
        public double Price
        {
            get {  return price;}
            set { price = value; }
        }
        private int qty;
        public int Qty
        {
            get { return qty;}
            set { qty = value; }
        }
        public IDMarketParam(int num, double val)
        {
            id = num;
            price = val;
            qty = 0;
        }
        public IDMarketParam(int num, int val)
        {
            id = num;
            price = 0;
            qty = val;
        }
    }
    public class SPuDS_Info
    {
        private IBApi.Contract contract;
        public IBApi.Contract Contract
        {
            get {return contract;}
            set {contract = value;}
        }
        private IBApi.Order order;
        public IBApi.Order Order
        {
            get { return order; }
            set { order = value; }
        }
        private int tickerid;
        public int TickerID
        { 
            get {return tickerid;}
            set {tickerid = value;}
        }
        public int OrderID
        {
            get { return order.OrderId; }
            set { order.OrderId = value; }
        }
        private SPuDS_Strategy strategy;
        public SPuDS_Strategy Strategy
        {
            get { return strategy; }
            set { strategy = value; }
        }
        public SPuDS_Info()
        {
            contract = new IBApi.Contract();
            order = new IBApi.Order();
            strategy = new SPuDS_Strategy();
        }
    }

    class SystemManager
    {
        private Samples.EWrapperImpl _IBManager;
        private static SystemManager s_inst = null;
        private SystemManager() { }
        private SystemManager(SystemManager sm) { }
        private Dictionary<String,SPuDS_Info> _SPuDS_Portfolio;
        private int NextTickerId = 1;
        private const int DAYS_OF_HIST_DATA = 1;
        private WebClient _WebClient;
        private Timer _pandlTimer;
  
        public event MessageUpdate _MainMessageBoxUpdate;
        public event SPuDSMessageUpdate _SPuDSMessageUpdate;
        public event SPuDSMarketUpdate _SPuDSMarketUpdate;
        public event EnableButton _Enable_AddSym;
        public event SPuDSPandLUpdate _PandLUpdate;
        public event SPuDSStateUpdate _StateUpdate;

        public static SystemManager CreateSystemManager()
        {
            if (s_inst == null)
            {
                s_inst = new SystemManager();
            }
            return s_inst;
        }
        public void CreateIBApi()
        {
            _IBManager = new Samples.EWrapperImpl();
            _IBManager._MainMessageUpdate += _IBManager__messageUpdate;
            _IBManager._OrderStatusMessageUpdate += _IBManager__OrderStatusMessageUpdate;
            _IBManager._MarketFeedUpdate += _IBManager__MarketFeedUpdate;
            _IBManager._StrategyUpdate += _IBManager__StrategyUpdate;

            _WebClient = new System.Net.WebClient();
        }

        public void StartSPuDS()
        {
            _SPuDS_Portfolio = new Dictionary<string, SPuDS_Info>();;
        }
        public void Connect()
        {
            _IBManager.ClientSocket.eConnect("127.0.0.1", 7496, 0);
            //_IBManager.ClientSocket.reqIds(10);
            
        }
        public void Disconnect()
        {
            _IBManager.ClientSocket.eDisconnect();
        }
        public void AddToSPuDS(String stock)
        {
            _SPuDS_Portfolio.Add(stock, new SPuDS_Info());
            AddToSPuDSAsync(stock);
        }
        public async Task AddToSPuDSAsync(String stock)
        {
            //Turn off Downloading
            _SPuDSMessageUpdate("Downloading Historical Data.\n", stock);
            await DownloadHistoricalData(stock);
            _SPuDSMessageUpdate("Historical Data Download Complete.\n", stock);
            _SPuDSMessageUpdate("Processing Historical Data.\n", stock);
            List<TradeDump> tradeData = new List<TradeDump>();
            await ProcessData(stock, tradeData);
            _SPuDSMessageUpdate("Historical Data Processing Complete.\n", stock);
            _SPuDSMessageUpdate("Training Strategy\n", stock);
            _SPuDS_Portfolio[stock].Strategy.ProcessHistoricalData(tradeData);
            _SPuDSMessageUpdate("Strategy Trained.\n", stock);
            _SPuDSMessageUpdate("Adding " + stock + " Market Feed.\n", stock);
            AddMarketFeed(stock, NextTickerId);
            NextTickerId++;
            _Enable_AddSym();
        }
        public void RemoveFromSPuDS(String stock)
        {
            CancelMarketFeed(stock);
            int curPos = _SPuDS_Portfolio[stock].Strategy.CurrentPosition;
            if (curPos < 0)
            {
                placeOrder(_SPuDS_Portfolio[stock], -curPos, "BUY");
            }
            else
            {
                placeOrder(_SPuDS_Portfolio[stock], curPos, "SELL");
            }

            _SPuDS_Portfolio.Remove(stock);
        }
        public void SPuDSClosePortfolio()
        {
            CancelAllMarketFeeds();
            
            foreach (String stk in _SPuDS_Portfolio.Keys)
            {
                int curPos = _SPuDS_Portfolio[stk].Strategy.CurrentPosition;
                if (curPos < 0)
                {
                    placeOrder(_SPuDS_Portfolio[stk], -curPos, "BUY");
                }
                else
                {
                    placeOrder(_SPuDS_Portfolio[stk], curPos, "SELL");
                }
            }
            _SPuDS_Portfolio.Clear();
        }
        public void setParticipationRates(double[] buy, double[] sell, int qty,String stock)
        {
            for (int i = 0;
                i < _SPuDS_Portfolio[stock].Strategy.NumOfStates;
                i++)
            {
                _SPuDS_Portfolio[stock].Strategy.Buy_ParticipationRate[i] = buy[i]/100.0;
                _SPuDS_Portfolio[stock].Strategy.Sell_ParticipationRate[i] = sell[i]/100.0;
            }
            _SPuDS_Portfolio[stock].Strategy.Max_Qty = qty;
        }

        //Functions for strategy
        private async Task DownloadHistoricalData(String stock)
        {
            for (int i = 0, offSet = 0; i < DAYS_OF_HIST_DATA;)
            {
                if (DateTime.Now.AddDays(-i - offSet).DayOfWeek == DayOfWeek.Sunday || DateTime.Now.AddDays(-i - offSet).DayOfWeek == DayOfWeek.Saturday)
                {
                    offSet++;
                    continue;
                }
                String date;
                if (DateTime.Now.Month < 10)
                {
                    if (DateTime.Now.AddDays(-i - offSet).Day < 10)
                    {
                        date = DateTime.Now.AddDays(-i - offSet).Date.Year.ToString() + "0" + DateTime.Now.AddDays(-i - offSet).Month.ToString() + "0" + DateTime.Now.AddDays(-i - offSet).Day.ToString();
                    }
                    else
                    {
                        date = DateTime.Now.AddDays(-i - offSet).Date.Year.ToString() + "0" + DateTime.Now.AddDays(-i - offSet).Month.ToString() + DateTime.Now.AddDays(-i - offSet).Day.ToString();
                    }
                }
                else
                {
                    if (DateTime.Now.Day - i - offSet < 10)
                    {
                        date = DateTime.Now.Date.Year.ToString() + DateTime.Now.Month.ToString() + "0" + (DateTime.Now.Day - i - offSet).ToString();
                    }
                    else
                    {
                        date = DateTime.Now.Date.Year.ToString() + DateTime.Now.Month.ToString() + (DateTime.Now.Day - i - offSet).ToString();
                    }
                }
                String url = "http://hopey.netfonds.no/tradedump.php?date=" + date + "&paper=" + stock + ".O&csv_format=csv";
                String fileName = i + "_" + stock + ".csv";
                _SPuDSMessageUpdate("Downloading " + (i + 1) + " of " + DAYS_OF_HIST_DATA + "\n",stock);
                await DownloadAsync(url, fileName);
                _SPuDSMessageUpdate("Download " + (i + 1) + " of " + DAYS_OF_HIST_DATA + " Complete\n", stock);
                i++;
            }
        }
        private async Task DownloadAsync(String url, String fileName)
        {
            try
            {
                using (_WebClient = new WebClient())
                {
                    await _WebClient.DownloadFileTaskAsync(new Uri( url ), Environment.CurrentDirectory.ToString() + "/" + fileName);
                }
            }
            catch (Exception)
            {
                _MainMessageBoxUpdate("Failed to Download " + fileName + " from " + url + ".\n");
            }
        }
        private async Task ProcessData(String stock, List<TradeDump> tradeData)
        {
            int index = 0;
            for (int i = 0; i < DAYS_OF_HIST_DATA; i++)
            {
                String file = i + "_" + stock + ".csv";
                using(StreamReader _reader = new StreamReader(file))
                {
                    
                    String line;
                    for (int k = 0; k < 55; k++)
                    {
                        _reader.Read();
                    }
                    while( (line =_reader.ReadLine()) != null)
                    {
                        int numOfCommmas = 0;
                        TradeDump td = new TradeDump();
                        String data = "";
                        foreach (Char c in line)
                        {
                            if (c == ',')
                            {
                                if (numOfCommmas == 0)
                                {
                                    td.Time = data;
                                }
                                else if (numOfCommmas == 1)
                                {
                                    td.Price = Convert.ToDouble(data);
                                }
                                else if (numOfCommmas == 2)
                                {
                                    td.Quantity = Convert.ToInt32(data);
                                }
                                else if (numOfCommmas == 5)
                                {
                                    tradeData.Add(td);
                                    numOfCommmas = -1;
                                    index++;
                                }
                                numOfCommmas++;
                                data = "";
                            }
                            else
                            {
                                data += c;
                            }
                        }
                    }
                }
                _SPuDSMessageUpdate("Processed " + (i+1) + " of " + DAYS_OF_HIST_DATA + ".\n", stock);
            }
        }

        //TWS Actions
        private void AddMarketFeed(String stock, int tickerId)
        {
            SPuDS_Info newStock = _SPuDS_Portfolio[stock];
            newStock.Contract.Symbol = stock;
            newStock.Contract.SecType = "STK";
            newStock.Contract.Currency = "USD";
            newStock.Contract.Exchange = "SMART";
            newStock.TickerID = tickerId;
            const String tickList = "";
            _IBManager.ClientSocket.reqMktData(newStock.TickerID,newStock.Contract,tickList, false, null);
        }
        private void CancelMarketFeed(String stock)
        {
            _IBManager.ClientSocket.cancelMktData(_SPuDS_Portfolio[stock].TickerID);
            _MainMessageBoxUpdate("MKT feed[" + stock + "] status: Closed");
        }
        private void CancelAllMarketFeeds()
        {
            foreach (SPuDS_Info info in _SPuDS_Portfolio.Values)
            {
                _IBManager.ClientSocket.cancelMktData(info.TickerID);
                _MainMessageBoxUpdate("MKT feed[" + info.Contract.Symbol + "] status: Closed");
            }
        }
        private void placeOrder(SPuDS_Info info, int Qty, String BuySell)
        {
            info.Order.OrderId = _IBManager.NextOrderId;
            info.Order.Action = BuySell;
            info.Order.TotalQuantity = Qty;
            info.Order.OrderType = "MKT";
            info.Order.Tif = "DAY";
            _IBManager.ClientSocket.placeOrder(info.Order.OrderId, info.Contract, info.Order);
            _IBManager.ClientSocket.reqIds(1);   
        }

        //Message Feeds
        private void _IBManager__messageUpdate(String message)
        {
            _MainMessageBoxUpdate(message);
        }
        private void _IBManager__MarketFeedUpdate(IDMarketParam idP)
        {
            String stock = null;
            foreach (String stk in _SPuDS_Portfolio.Keys)
            {
                if (_SPuDS_Portfolio[stk].TickerID == idP.ID)
                {
                    stock = stk;
                    break;
                }
            }
            if (stock != null)
            {
                _SPuDSMarketUpdate(idP.Price, stock);
            }
        }
        private void _IBManager__OrderStatusMessageUpdate(IDMessage idM)
        {
            String stock = null;
            foreach (String stk in _SPuDS_Portfolio.Keys)
            {
                if (_SPuDS_Portfolio[stk].OrderID == idM.ID)
                {
                    stock = stk;
                    break;
                }
            }
            if (stock != null)
            {
                _SPuDSMessageUpdate(idM.Message, stock);
            }

        }
        private void _IBManager__StrategyUpdate(IDMarketParam idP)
        {
            String stock = null;
            bool trade = false;
            foreach (String stk in _SPuDS_Portfolio.Keys)
            {
                if (_SPuDS_Portfolio[stk].TickerID == idP.ID)
                {
                    stock = stk;
                    break;
                }
            }

            if (stock != null)
            {
                if (idP.Qty == 0)
                {
                    trade = _SPuDS_Portfolio[stock].Strategy.Update(idP.Price);
                    _PandLUpdate(_SPuDS_Portfolio[stock].Strategy.getPandL, stock);
                }
                else
                {
                    trade = _SPuDS_Portfolio[stock].Strategy.Update(idP.Qty);
                }
            }
            if (trade)
            {
                int curPos = _SPuDS_Portfolio[stock].Strategy.CurrentPosition;
                int qty = _SPuDS_Portfolio[stock].Strategy.getQtyDiff();
                String buysell;
                if (qty < 0)
                {
                    if (curPos > 0)
                    {
                        placeOrder(_SPuDS_Portfolio[stock], curPos, "SELL");
                        qty += curPos;
                    }
                    qty = -qty;
                    buysell = "SSELL";
                }
                else
                {
                    if (curPos < 0)
                    {
                        placeOrder(_SPuDS_Portfolio[stock], -curPos, "BUY");
                    }
                    buysell = "BUY";
                }
                MarketState curState = _SPuDS_Portfolio[stock].Strategy.CurrentMarketState;
                if (curState == MarketState.UP)
                {
                    _StateUpdate("UP", stock);
                }
                else if (curState == MarketState.UpNEUTRAL)
                {
                    _StateUpdate("UPNeutral", stock);
                }
                else if (curState == MarketState.DownNEUTRAL)
                {
                    _StateUpdate("DOWNNeutral", stock);
                }
                else
                {
                    _StateUpdate("DOWN", stock);
                }
                placeOrder(_SPuDS_Portfolio[stock], qty, buysell);
            }
        }
       
    } 
}
