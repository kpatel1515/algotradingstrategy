﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AlgoTradingStrat
{
    public delegate void ParticipationRatePass(double[] buy, double[] sell, int Qty ,String Stock);
    public partial class SPuDS_StratUI : UserControl
    {
        private String tempText;
        private double[] buy_participationrates;
        private double[] sell_participationrates;

        public event ParticipationRatePass ParticipationRatesUpdated;

        public SPuDS_StratUI()
        {
            InitializeComponent();

            sell_participationrates = new double[7];
            buy_participationrates = new double[7];

            textBox_BDown.Enter += textBox_Enter;
            textBox_BDown.Enter += textBox_Enter;
            textBox_BDownNeutral.Enter += textBox_Enter;
            textBox_BUpNeutral.Enter += textBox_Enter;
            textBox_BUp.Enter += textBox_Enter;

            textBox_SDown.Enter += textBox_Enter;
            textBox_SDown.Enter += textBox_Enter;
            textBox_SDownNeutral.Enter += textBox_Enter;
            textBox_SUpNeutral.Enter += textBox_Enter;
            textBox_SUp.Enter += textBox_Enter;

            textBox_BDown.Leave += textBox_Leave;
            textBox_BDown.Leave += textBox_Leave;
            textBox_BDownNeutral.Leave += textBox_Leave;
            textBox_BUpNeutral.Leave += textBox_Leave;
            textBox_BUp.Leave += textBox_Leave;

            textBox_SDown.Leave += textBox_Leave;
            textBox_SDown.Leave += textBox_Leave;
            textBox_SDownNeutral.Leave += textBox_Leave;
            textBox_SUpNeutral.Leave += textBox_Leave;
            textBox_SUp.Leave += textBox_Leave;

        }

        private void textBox_Enter(object sender, EventArgs e)
        {
            TextBox tempTextBox = sender as TextBox;
            tempText = tempTextBox.Text;
            tempTextBox.Text = "";
        }
        private void textBox_Leave(object sender, EventArgs e)
        {
            TextBox tempTextBox = sender as TextBox;
            double val;
            if (double.TryParse(tempTextBox.Text, out val))
            {
                if (0.0 <= val && val <= 100.0)
                {
                    tempTextBox.Text = val.ToString();
                }
            }
            else
            {
                tempTextBox.Text = tempText;
            }
        }
        private void Text_Selection(object sender, EventArgs e)
        {
            numericUpDown_Qty.Select(0,numericUpDown_Qty.Text.Length);
        }
        public void button_UpdateRate_Click(object sender, EventArgs e)
        {
            button_UpdateRate.Enabled = false;
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    TextBox tb = c as TextBox;
                    if (tb.Name[8] == 'S' || tb.Name[8] == 'B')
                    {
                        int pos = Int32.Parse((String)tb.Tag);
                        if (tb.Name[8] == 'B')
                        {
                            buy_participationrates[pos] = Convert.ToDouble(tb.Text);
                        }
                        else
                        {
                            sell_participationrates[pos] = Convert.ToDouble(tb.Text);
                        }
                    }
                }
            }
            int Qty = Convert.ToInt32(numericUpDown_Qty.Value);
            ParticipationRatesUpdated(buy_participationrates, sell_participationrates,Qty,this.Name.Substring(6));
            button_UpdateRate.Enabled = true;
        }
        

        public int get_Qty()
        {
            return Convert.ToInt32(numericUpDown_Qty.Value);
        }
        public double get_PandL()
        {
            return Convert.ToDouble(textBox_PandL.Text);
        }
        public void set_PandL(double newPandL)
        {
            textBox_PandL.Text = Math.Round(newPandL,2).ToString();
        }
        public void set_State(String state)
        {
            textBox_currentState.Text = state;
        }
        public void UpdateChart_Price(double price)
        {
            chart_PriceHist.Series[0].Points.AddXY(DateTime.Now.ToOADate(), price);

            double max = Math.Ceiling(chart_PriceHist.Series[0].Points.FindMaxByValue("Y1").YValues[0]);
            double min = Math.Floor(chart_PriceHist.Series[0].Points.FindMinByValue("Y1").YValues[0]);

            chart_PriceHist.ChartAreas[0].AxisY.Maximum = max + (max - min) / 5;
            chart_PriceHist.ChartAreas[0].AxisY.Minimum = min - (max - min) / 5;

            chart_PriceHist.ChartAreas[0].AxisY.Interval = Math.Round((max - min) / 5,1);
            chart_PriceHist.ChartAreas[0].AxisX.Interval = 15;

            double removetime = DateTime.Now.AddSeconds(-180.0).ToOADate();

            if (chart_PriceHist.Series[0].Points[0].XValue < removetime)
            {
                chart_PriceHist.Series[0].Points.RemoveAt(0);
            }

            chart_PriceHist.ChartAreas[0].AxisX.Minimum = chart_PriceHist.Series[0].Points[0].XValue;
            chart_PriceHist.ChartAreas[0].AxisX.Maximum = DateTime.FromOADate(chart_PriceHist.Series[0].Points[0].XValue).AddSeconds(185.0).ToOADate();
            chart_PriceHist.Invalidate();
        }
        public void addMessage(String message)
        {
            //textBox_Messages.AppendText(message);
            textBox_Messages.Select(0, 0);
            textBox_Messages.SelectedText = message + Environment.NewLine;
        }
    }
}
